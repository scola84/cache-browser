'use strict';

const EventHandler = require('@scola/events');

class BrowserCache extends EventHandler {
  constructor(session, local) {
    super();

    this.sessionStorage = session;
    this.localStorage = local;

    this.cache = session;
    this.hash = null;
  }

  getHash() {
    return this.hash;
  }

  setHash(hash) {
    this.hash = hash;
    return this;
  }

  local() {
    this.cache = this.localStorage;
    return this;
  }

  isLocal() {
    return this.cache === this.localStorage;
  }

  toLocal(remove) {
    const values = this.getItem(this.hash, this.sessionStorage);
    this.setItem(this.hash, values, this.localStorage);

    if (remove !== false) {
      this.removeItem(this.hash, this.sessionStorage);
    }

    return Promise.resolve(values);
  }

  session() {
    this.cache = this.sessionStorage;
    return this;
  }

  isSession() {
    return this.cache === this.sessionStorage;
  }

  toSession(remove) {
    const values = this.getItem(this.hash, this.localStorage);
    this.setItem(this.hash, values, this.sessionStorage);

    if (remove !== false) {
      this.removeItem(this.hash, this.localStorage);
    }

    return Promise.resolve(values);
  }

  delete(key) {
    const values = this.getItem(this.hash);
    delete values[key];

    this.setItem(this.hash, values);
    this.emit('change', values);

    return Promise.resolve(values);
  }

  deleteAll() {
    this.removeItem(this.hash);
    this.emit('change');

    return Promise.resolve();
  }

  get(key) {
    const values = this.getItem(this.hash);
    return Promise.resolve(values && values[key]);
  }

  getAll() {
    return Promise.resolve(this.getItem(this.hash));
  }

  set(key, value) {
    const values = this.getItem(this.hash);
    values[key] = value;

    this.setItem(this.hash, values);
    this.emit('change', values);

    return Promise.resolve(values);
  }

  setAll(values) {
    this.setItem(this.hash, values);
    this.emit('change', values);

    return Promise.resolve(values);
  }

  getItem(key, cache) {
    cache = cache || this.cache;
    return JSON.parse(cache.getItem(key));
  }

  removeItem(key, cache) {
    cache = cache || this.cache;
    cache.removeItem(key);
  }

  setItem(key, value, cache) {
    cache = cache || this.cache;
    cache.setItem(key, JSON.stringify(value));
  }
}

module.exports = BrowserCache;
