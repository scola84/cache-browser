'use strict';

const DI = require('@scola/di');
const Cache = require('./lib/cache');

class Module extends DI.Module {
  configure() {
    this.inject(Cache).with(
      this.value(sessionStorage),
      this.value(localStorage)
    );
  }
}

module.exports = {
  Cache,
  Module
};
